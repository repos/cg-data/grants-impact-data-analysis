# Notebook setup 

## Imports

### Utils
import os
import dotenv

import pprint
import warnings


### Data collection
import wmfdata as wmf
from wmfdata import mariadb, hive, spark
from wmfdata.utils import pct_str, pd_display_all

from pyspark.sql import DataFrame, SparkSession

import requests
from urllib import request


### Data processing
from unidecode import unidecode
import re
import json
from datetime import datetime, timedelta, date


### Data science & math
import pandas as pd
import numpy as np


### Data visualization
import plotly.graph_objects as go


### Other




## Configurations
### Pandas
pd.set_option('display.max_columns', None)

### Warnings
# Suppress pandas SQLAlchemy connection warnings coming from wmfdata.mariadb
warnings.filterwarnings("ignore", category=UserWarning, message='.*pandas only supports SQLAlchemy connectable.*')

#warnings.resetwarnings()


## Constants
INPUT_DIR = 'input/'
OUTPUT_DIR = 'output/'
PUBLIC_OUTPUT_DIR = 'public_output/'


## Load .env (repo root level)
CONFIG = dotenv.dotenv_values(".env")


## Local functions
def print_spark_session_info(spark: SparkSession):
    """
    Prints helpful information about a wmfdata PySpark session for documentation and monitoring.
    Credit: AndrewTavis_WMDE in Phabricator comment https://phabricator.wikimedia.org/T315024#9729192
    """
    print(f"Spark version: {spark.sparkContext.version}")
    print(f"Spark app name: {spark.sparkContext.appName}")
    print(
        f"Scheduler UI: https://yarn.wikimedia.org/proxy/{spark.sparkContext.applicationId}"
    )