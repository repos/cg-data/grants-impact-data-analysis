# Events Impact Reporting datasets generation

## Description
This repo contains the code that generates the datasets used by the Events Impact Reporting web app.

For more details and guides, see the [documentation folder](https://gitlab.wikimedia.org/repos/cg-data/events-impact-reporting-datasets-generation/-/tree/main/documentation?ref_type=heads).

## Key stakeholders  

### Direct stakeholders  
- Wikimedia Foundation staff who use the generated datasets to maintain the Events Impact Reporting web app  
- The public can directly access and use the generated datasets  

### Indirect stakeholders
- The people who organize, engage with or participate in events and grants supported by the Wikimedia Foundation
- Wikimedia Foundation staff who use the insights based on the generated datasets to inform stategy and planning

## Roadmap
Presently, this code is run manually every month. In the future, we may set up a monthly scheduled job to run this data pull via Airflow.

## License

### Code

MIT License

Copyright (c) 2024 Wikimedia Foundation

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

### Content
This work is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1).
