# How to use the generated csv datasets
 
The generated csv datasets can be used to analyse the most current data, or they can be used to reproduce prior analyses with the data that was most current at the time of the prior analyses.

#### To select the most current data for analyses:
- Select only the ended events files for historical data and both ended and ongoing events data for the most recent processing month. 
  - I.e., files ending with `_ended.csv` for all processing months of interest, plus the file ending with `_ongoing.csv` for the most recent processing month.

#### To select previously used data for analysis reproducibility:
- Select only the ended events files for historical data and both ended and ongoing events data for the processing month of interest. 
  - For example, let's say we want to get the data to reproduce an analysis that was done on the most current data in September 2024. In September 2024, the most recent processing month for this data would be August 2024 (2024-08). So, to get the relevant data, we can select all files ending with `_ended.csv` dated `2024_08` and earlier, plus the file named `events_2024_08_ongoing.csv`.