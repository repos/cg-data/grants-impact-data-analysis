# Data risk mitigation  
This document describes the risk mitigation measures implemented in this project.

## Data risk mitigation measures by data aggregation level
- **for all aggregation levels**
  - generalize `participants_count` values if smaller than [the granular threshold](https://foundation.wikimedia.org/wiki/Legal:Data_publication_guidelines#Threshold_table)
- **by year**
  - use all data
- **by month**
  - use all data
- **by wiki project**
  - use all data
- **by Wikimedia Foundation region**
  - use all data
- **by grant**
  - exclude events associated with countries on [the Wikimedia Foundation country and territory protection list](https://foundation.wikimedia.org/wiki/Legal:Country_and_Territory_Protection_List)
- **by country**
  - aggregate events associated with high data risk classification countries to Wikimedia Foundation regions
- **by event**
  - exclude events associated with high data risk classification countries
  - exclude events with `participants_count` values smaller than [the granular threshold](https://foundation.wikimedia.org/wiki/Legal:Data_publication_guidelines#Threshold_table)
- **by event pairs**
  - exclude if either of events have are associated with high data risk classification countries
  - exclude events with `participants_count` values smaller than [the granular threshold](https://foundation.wikimedia.org/wiki/Legal:Data_publication_guidelines#Threshold_table)


## Data risk mitigation measures descriptions
- **generalize `participants_count` values if smaller than [the granular threshold](https://foundation.wikimedia.org/wiki/Legal:Data_publication_guidelines#Threshold_table)**:
  - The current threshold for editors as of Sept 2024 is 25
  - Generalization is done by reporting the participants_count numbers less than the threshold as “<25”  
- **exclude events with `participants_count` values smaller than [the granular threshold](https://foundation.wikimedia.org/wiki/Legal:Data_publication_guidelines#Threshold_table)**:
  - The current threshold for editors as of Sept 2024 is 25
  - Exclude events with the participants_count numbers less than the threshold
- **exclude events associated with countries on [the Wikimedia Foundation country and territory protection list](https://foundation.wikimedia.org/wiki/Legal:Country_and_Territory_Protection_List)**:
  - exclude events that have:
    - at least one of event or grant countries data risk classification not missing, AND the highest data risk classification among them is higher than the "Lower risk" classification
    - OR both event and grant countries data risk classification are missing AND `event_meeting_type` is "in-person" or "both"
- **aggregate events associated with countries on [the Wikimedia Foundation country and territory protection list](https://foundation.wikimedia.org/wiki/Legal:Country_and_Territory_Protection_List) to Wikimedia Foundation regions**:
  - aggregate the values for high risk data by Wikimedia Foundation regions and report the aggregated values
   

## Reference
- [Wikimedia Foundation Legal Data Publication Guidelines](https://foundation.wikimedia.org/wiki/Legal:Data_publication_guidelines)
- [Wikimedia Foundation Legal Country and Territory Protection List](https://foundation.wikimedia.org/wiki/Legal:Country_and_Territory_Protection_List)
- [Security Policy Risk Management on Office Wiki (restricted access)](https://office.wikimedia.org/wiki/Security/Policy/Risk_Management)